package com.test.kursy

import android.os.AsyncTask
import android.util.Log

class CustomAsyncTask : AsyncTask<String, Int, String>() {

    override fun doInBackground(vararg params: String?): String {
        Log.i("doInBackground", "Я в бэкграунде")
        return "Ушел из бэкграунда"
    }

    override fun onPreExecute() {
        Log.i("onPreExecute", "onPreExecute")
        super.onPreExecute()
    }

    override fun onPostExecute(result: String?) {
        Log.i("onPostExecute", result)
        super.onPostExecute(result)
    }

}