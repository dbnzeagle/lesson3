package com.test.kursy

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import kotlin.concurrent.fixedRateTimer
import kotlin.concurrent.timer

class DialogLoader : DialogFragment() {
    companion object {
        const val TAG = "DialogLoader"
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_loader, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        timer("Timer", false, 1000, 10)
        { this@DialogLoader.dismiss() }
    }
}