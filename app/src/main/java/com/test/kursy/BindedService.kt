package com.test.kursy

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder


class BindedService : Service() {
    val binder = LocalBinder()


    class LocalBinder : Binder() {

        fun getService(): BindedService {
            return BindedService()
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    fun getString(): String {
        return "Из Сервиса"
    }
}