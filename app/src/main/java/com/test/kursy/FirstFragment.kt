package com.test.kursy

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_first.*

class FirstFragment : Fragment() {

    companion object {
        fun newInstance(test: String): FirstFragment {
            val fragmentInstance = FirstFragment()
            val arguments = fragmentInstance.arguments
            arguments?.putString("TEST", test)
            return fragmentInstance
        }

        fun newInstance2(test: String) = FirstFragment().apply {
            arguments = bundleOf("TEST" to test)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val data = listOf<CustomColor>(
            CustomColor("blue", R.color.blue),
            CustomColor("yellow", R.color.yellow),
            CustomColor("red", R.color.red),
            CustomColor("grey", R.color.grey),
            CustomColor("green", R.color.green),
            CustomColor("blue", R.color.blue),
            CustomColor("yellow", R.color.yellow),
            CustomColor("red", R.color.red),
            CustomColor("grey", R.color.grey),
            CustomColor("blue", R.color.blue),
            CustomColor("yellow", R.color.yellow),
            CustomColor("red", R.color.red),
            CustomColor("grey", R.color.grey)
        )
        Log.i("TEST", arguments?.getString("TEST").toString())
        fragment_first_recycler.layoutManager = LinearLayoutManager(context)
        fragment_first_recycler.adapter = MyAdapter(data)
    }
}