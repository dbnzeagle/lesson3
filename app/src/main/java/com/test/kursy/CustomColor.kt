package com.test.kursy

data class CustomColor(
    val title: String,
    val id: Int
)