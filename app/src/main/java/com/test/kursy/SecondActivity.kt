package com.test.kursy

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_second.*
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.URL
import java.util.*
import kotlin.concurrent.timer


class SecondActivity : AppCompatActivity(), TestInterface {
    val CONSTANT_URL =
        "https://images.freeimages.com/images/small-previews/7e9/ladybird-1367182.jpg"
    val REQUEST_URL = "https://test.kode-t.ru/recip"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

//        startService(Intent(this, CustomService::class.java))


//        val intent = Intent(this, BindedService::class.java)
//        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)

//        CustomAsyncTask().execute("Выполнить")

        createThread()
    }

    var serviceConnection = object : ServiceConnection {

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            val binded = service as BindedService.LocalBinder
            Log.i("STRING", binded.getService().getString())
        }

        override fun onServiceDisconnected(name: ComponentName?) {

        }

    }

    fun getResponseData(urlString: String): String? {
        val url = URL(urlString)
        val connection = url.openConnection()
        var inputStream: BufferedReader? = null
        try {
            inputStream = BufferedReader(InputStreamReader(connection.getInputStream()))
        } catch (e: Throwable) {
            Log.i("ERR", e.message)
        }
        return inputStream?.readLine()
    }

    fun getStreamData(urlString: String): InputStream {
        val url = URL(urlString)
        return url.openConnection().getInputStream()
    }

    fun getImageFromStream(stream: InputStream): Bitmap {
        return BitmapFactory.decodeStream(stream)
    }

    fun createThread() {
        Thread(Runnable {
            val data = getStreamData(CONSTANT_URL)
            val bitmap = getImageFromStream(data)
            val responseData = getResponseData(REQUEST_URL)
            textView2.post {
                textView2.text = responseData
            }
            imageView2.post(Runnable {
                imageView2.setImageBitmap(bitmap)
            })
        }).start()


    }

    lateinit var lateVal: Bitmap
    override fun test(stream: InputStream) {
        val immutableVal: String = this.CONSTANT_URL
//        var mutableVal: ActionBar? = null
        lateVal = BitmapFactory.decodeStream(stream)
        val test = lateVal.byteCount
        val value = if(test.equals("1")){}else{}
    }
}
