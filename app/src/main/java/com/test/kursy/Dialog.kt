package com.test.kursy

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.dialog_custom.*

class Dialog : DialogFragment() {
    companion object {
        const val TAG = "CustomDialog"

        fun newInstance(test: String) = Dialog().apply {
            arguments = bundleOf("TEST" to test)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_custom, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.i("Dialog Message",arguments?.getString("TEST","-1"))
        dialog_cancel.setOnClickListener {
            Log.i("Cancel", "Cancel")
        }
        dialog_ok.setOnClickListener {
            Log.i("Ok", "Ok")
        }
    }
}