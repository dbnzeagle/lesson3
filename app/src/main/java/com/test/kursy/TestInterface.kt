package com.test.kursy

import java.io.InputStream

interface TestInterface {
    fun test(stream: InputStream)
}