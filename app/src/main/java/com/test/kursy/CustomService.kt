package com.test.kursy

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log

class CustomService : Service() {
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        Log.i("Service","OnCreate")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        stopSelf()
        return super.onStartCommand(intent, flags, startId)
    }
}