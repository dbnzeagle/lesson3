package com.test.kursy

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.util.AttributeSet
import android.util.Log
import android.view.ContextMenu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.SeekBar
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.test.kursy.R.*

import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.abs

class MainActivity : AppCompatActivity() {

    var counter: Int = 0
    val STRING_KEY = "STRING_KEY"
    val INT_KEY = "INT_KEY"
    val R_CODE = 1

    lateinit var viewPager: ViewPager2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.activity_main)
        Log.i("onCreate", "onCreate")

        val list = listOf<String>(
            "Привет",
            "Привет2",
            "Привет3",
            "Привет4",
            "Привет5",
            "Привет6",
            "Привет7",
            "Привет8"
        )

        viewPager = view_pager
        viewPager.adapter = ViewPager2Adapter(list)
        val nextPosition = 24
        val currentMargin = 4
        val pageTranslation = nextPosition + currentMargin
        viewPager.setPageTransformer { page, position ->
            when {
                position < -1 -> {
                    page.alpha = 0f
                }
                position <= 1 -> {
                    page.translationX = -pageTranslation * position
                    val scaleFactor = 1 - (0.05f * abs(position))
                    page.alpha = (0.2f + (((1 - (0.1f * abs(position)) - 0.85f) / (1f - 0.85f))))
                }
                else -> {
                    page.alpha = 0f
                }
            }
        }
        val itemDecorator = HorizontalMarginItemDecorator(16)
        viewPager.addItemDecoration(itemDecorator)
    }


    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
//        val menu = menuInflater
        menuInflater.inflate(R.menu.menu_main, menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> {
                Log.i("action_settings", "action_settings")
            }
            R.id.action_back -> {
                Log.i("action_back", "action_back")
            }
        }
        return super.onContextItemSelected(item)
    }
}
